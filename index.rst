.. Test for GitLab CI/CD for a Sphinx-doc website documentation master file, created by
   sphinx-quickstart on Tue Jul 18 18:09:36 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Test GitLab CI/CD of a Sphinx-doc website's documentation!
=====================================================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:
   :glob:

   *


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
