# syntax=docker/dockerfile:1
FROM python:3.9.17-bookworm
RUN pip3 install --verbose --upgrade pip; \
    pip3 install --verbose --force-reinstall "sphinx==6.2.1"; \
    pip3 install --verbose sphinx_design "sphinx==6.2.1"; \
    pip3 install --verbose --force-reinstall "numpy==1.19.4"; \
    pip3 install --verbose sphinxext.opengraph "sphinx==6.2.1"; \
    pip3 install --verbose sphinx_comments "sphinx==6.2.1"; \
    pip3 install --verbose linkify-it-py "sphinx==6.2.1"; \
    pip3 install --verbose furo "sphinx==6.2.1"; \
    pip3 install --verbose myst-parser "sphinx==6.2.1"